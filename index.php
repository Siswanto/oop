<?php

require('animal.php');
require('ape.php');
require('frog.php');


$sheep = new Animal("shaun");

echo "Animal name : $sheep->name <br>"; // "shaun"
echo "Animal Legs : $sheep->legs <br>"; // 4
echo "Cold blooded :  $sheep->cold_blooded <br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$sungokong = new Ape("kera sakti");

echo "Animal name : $sungokong->name <br>"; // "kera sakti"
echo "Animal Legs : $sungokong->legs <br>"; // 2
echo "Cold blooded :  $sungokong->cold_blooded"; // "no"
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");

echo "Animal name : $kodok->name <br>"; // "kera sakti"
echo "Animal Legs : $kodok->legs <br>"; // 2
echo "Cold blooded :  $kodok->cold_blooded"; // "no"
$kodok->jump() ; // "hop hop"